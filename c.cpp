#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

int main(){
  int N, K;
  while (cin >> N){
    cin >> K;
    int num;
    vector<int> nums;
    unordered_map<int, int> places;
    for (int i = 0; i < N; i++){
      cin >> num;
      nums.push_back(num);
      places[num] = i;
    }
    int index = 0;
    int start = N;
    int swaps = 0;
      for (int j = start; j > 0; j--){
        //cout << j << " " << places[j] << " " << index << endl;
        if (places[j] != index){
          int d = places[j];
          int e = nums[index];
          nums[places[j]] = nums[index];
          nums[index] = j;
          places[j] = index;
          places[e] = d;
          swaps++;
        }
        if (swaps == K)
          break;
        index++;
      }
    int printSpace = 0;
    for (int i = 0; i < nums.size(); i++){
      cout << nums[i];
      if (i != nums.size() - 1)
        cout << " ";
    }
    cout << endl;
  }
}
