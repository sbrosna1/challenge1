#include <iostream>
#include <vector>
#include <iomanip>
using namespace std;

int main(){
  vector<int> coins = {5, 10, 50, 100, 200, 500, 1000, 5000, 10000};
  double amount;
  while (cin >> amount){
    int intAmount = amount * 100;
    vector<unsigned long int> numWays(intAmount + 1, 0);
    numWays[0] = 1;
    numWays[1] = 1;
    numWays[2] = 1;
    numWays[3] = 1;
    numWays[4] = 1;
    for (auto coin : coins){
      for (int i = coin; i <= intAmount; i++){
        numWays[i] += numWays[i - coin];
      }
    }
    cout << fixed;
    cout << setprecision(2);
    cout << setw(6) << amount;
    cout << setprecision(0)  << right << setw(17) << numWays[intAmount] << endl;
  }
}
