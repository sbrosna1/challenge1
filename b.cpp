#include <iostream>
#include <unordered_map>
#include <unordered_set>
using namespace std;

int main(){
  string a, b;
  while (cin >> a){
    cin >> b;
    unordered_map<char, char> mapping;
    unordered_set<char> mapped;
    int iso = 1;
    if (a.length() != b.length()){
      iso = 0;
    }
    else{
      for (int i = 0; i < a.length(); i++){
        if (mapping.find(a[i]) == mapping.end()){
          if (mapped.find(b[i]) == mapped.end()){
            mapping[a[i]] = b[i];
          } else {
            iso = 0;
            break;
          }
        } else if (mapping[a[i]] != b[i]){
          iso = 0;
          break;
        }
      }
    }
    if (iso)
      cout << "Isomorphic" << endl;
    else
      cout << "Not Isomorphic" << endl;
  }
}
