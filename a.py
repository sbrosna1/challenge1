import sys

for line in sys.stdin:
	line = line.split(">")
	line[0] = line[0].replace("A ","")
	for i in range(len(line)):
		if line[i][0] != "<":
			for c in line[i]:
				if c == '<':
					break
				else:
					line[i] = line[i][1:]
		line[i] = line[i].replace("<","")
		l = len(line)

	if line[-1] == "":	
		line.remove("")

	head = 0
	tail = len((line)) - 1
	isBalanced = True

	if len(line) % 2 == 1:
		isBalanced = False
	
	stack = []
	for curr_tag in line:
		if curr_tag[0] != "/":
			stack.append(curr_tag)
		else:
			if len(stack) == 0:
				isBalanced = False
				break
			pop = stack.pop()
			if curr_tag != "/" + pop:
				isBalanced = False
				break
	
	if isBalanced:
		print("Balanced")
	else:
		print("Unbalanced")



