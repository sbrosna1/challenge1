import sys

rows = map(lambda line: map(int, line.split()), sys.stdin)	

for i in range(0,len(rows),2):
	row1 = sorted(rows[i])
	row2 = sorted(rows[i+1])
	
	valid = True
	# see if row1 can be infront of row2
	for j in range(len(row1)):
		if row1[j] >= row2[j]:
			valid = False
			break
	if valid:
		print("Yes")
	else:
		print("No")

	
	
