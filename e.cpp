#include <iostream>
#include <list>
#include <vector>
using namespace std;

void generateStrings(list<char> &str, int ones, int zeros, int N){
  if (str.size() == N){
    for (auto bit : str)
      cout << bit;
    cout << endl;
    return;
  }

  if (zeros){
    str.push_back('0');
    generateStrings(str, ones, zeros - 1, N);
    str.pop_back();
  }

  if (ones){
    str.push_back('1');
    generateStrings(str, ones - 1, zeros, N);
    str.pop_back();
  }
}

int main(){
  int N, K, newLine = 0;
  while (cin >> N){
    if (newLine++)
      cout << endl;
    cin >> K;
    int ones = K, zeros = N - K;
    list<char> str;
    generateStrings(str, ones, zeros, N);
  }
}
